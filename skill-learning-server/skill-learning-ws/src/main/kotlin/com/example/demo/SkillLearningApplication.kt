package com.lmb.skill.learning

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SkillLearningApplication

fun main(args: Array<String>) {
	runApplication<DemoApplication>(*args)
}
